package com.xuege.springboot.spi.aware.beanname;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: bean名称回调,仅限于当前对象
 * @create: 2021/5/16 16:46
 */
@Component
public class TestBeanNameAware implements BeanNameAware {
    private static String beanName;
    @Override
    public void setBeanName(String beanName) {
        TestBeanNameAware.beanName = beanName;
    }
    public static String getBeanName() {
        return beanName;
    }
}
