package com.xuege.springboot.spi.environmentpostprocessor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: 加载本地自定义配置.需要在META-INF下面新建spring.factories配置文件
 * @create: 2021/5/16 16:20
 */
public class AppEnvironmentPostProcessor implements EnvironmentPostProcessor {
    /** 本地自定义配置 */
    private static final Properties p = new Properties();
    private String[] profiles = {"app.properties"};
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        for (String profile : profiles) {
            environment.getPropertySources().addLast(loadProperties(new ClassPathResource(profile)));
        }
    }
    private PropertySource<?> loadProperties(Resource resource) {
        if (!resource.exists()) {
            throw new IllegalArgumentException("resource ["+resource.getFilename()+"] is not found");
        }
        try {
            p.load(resource.getInputStream());
            return new PropertiesPropertySource(Objects.requireNonNull(resource.getFilename()),p);
        } catch (IOException e) {
            throw new IllegalArgumentException("load properties error,cause:"+e.getCause());
        }
    }
}
