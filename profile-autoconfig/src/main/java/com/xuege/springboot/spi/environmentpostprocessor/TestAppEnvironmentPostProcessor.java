package com.xuege.springboot.spi.environmentpostprocessor;

import com.xuege.springboot.spi.aware.applicationcontext.TestApplicationContextAware;
import com.xuege.springboot.spi.aware.beanname.TestBeanNameAware;
import com.xuege.springboot.spi.aware.environment.TestEnvironmentAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description:测试环境配置
 * @create: 2021/5/16 16:29
 */
@Component
@Order(-1)
public class TestAppEnvironmentPostProcessor  {
    @Value("${app.name}")
    private String appName;
    @Value("${app.id}")
    private int appId;
    @PostConstruct
    public void test() {
        System.out.println("获取到了appName为:"+appName+",获取到的appId为:"+appId);
        ApplicationContext app = TestApplicationContextAware.getApp();
        System.out.println(app);
        Environment env = TestEnvironmentAware.getEnv();
        System.out.println(env);
        String beanName = TestBeanNameAware.getBeanName();
        System.out.println(beanName);
    }

}
