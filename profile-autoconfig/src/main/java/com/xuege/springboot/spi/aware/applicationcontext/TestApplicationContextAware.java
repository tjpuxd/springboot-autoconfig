package com.xuege.springboot.spi.aware.applicationcontext;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
/*
 * @author: 薛棣
 * @version: v1.0
 * @description: 获取上下文对象
 * @create: 2021/5/16 16:36
 */
@Component
public class TestApplicationContextAware implements ApplicationContextAware {
    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        TestApplicationContextAware.applicationContext = applicationContext;
    }
    public static ApplicationContext getApp() {
        return applicationContext;
    }
}
