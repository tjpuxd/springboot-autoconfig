package com.xuege.springboot.spi.aware.environment;

import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: 获取上下文配置相关
 * @create: 2021/5/16 16:40
 */
@Component
public class TestEnvironmentAware implements EnvironmentAware {
    private static Environment environment;
    @Override
    public void setEnvironment(Environment environment) {
        TestEnvironmentAware.environment = environment;
    }
    public static Environment getEnv() {
        return environment;
    }
}
