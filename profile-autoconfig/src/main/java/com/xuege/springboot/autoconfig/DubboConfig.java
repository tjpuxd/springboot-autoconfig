package com.xuege.springboot.autoconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: dubbo配置相关
 * @create: 2021/5/16 17:05
 */
@Configuration
@EnableConfigurationProperties({
        DubboAppProperties.class,DubboRegisterProperties.class,DubboScanProperties.class
})
public class DubboConfig {
    @Autowired
    private DubboAppProperties dubboAppProperties;
    @Autowired
    private DubboRegisterProperties dubboRegisterProperties;
    @Autowired
    private DubboScanProperties dubboScanProperties;
    @PostConstruct
    public void initConfigration() {
        int id = dubboAppProperties.getId();
        String name = dubboAppProperties.getName();
        String ip = dubboRegisterProperties.getIp();
        int port = dubboRegisterProperties.getPort();
        String protocol = dubboRegisterProperties.getProtocol();
        String basePackage = dubboScanProperties.getBasePackage();
        System.out.println(String.format("获取到的id:%s,name:%s,ip:%s,port:%s,protocol:%s,basePackage:%s",
                id,name,ip,port,protocol,basePackage));
    }
}
