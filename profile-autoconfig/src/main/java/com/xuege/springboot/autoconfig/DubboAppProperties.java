package com.xuege.springboot.autoconfig;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: dubbo应用配置相关
 * @create: 2021/5/16 17:00
 */
@Data
@ConfigurationProperties(prefix = "dubbo.application")
public class DubboAppProperties {
    private String name;
    private int id;
}
