package com.xuege.springboot.autoconfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: dubbo配置扫描相关
 * @create: 2021/5/16 17:04
 */
@Data
@ConfigurationProperties(prefix = "dubbo.scan")
public class DubboScanProperties {
    private String basePackage;
}
