package com.xuege.springboot.autoconfig;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/*
 * @author: 薛棣
 * @version: v1.0
 * @description: dubbo注册中心相关
 * @create: 2021/5/16 17:02
 */
@Data
@ConfigurationProperties(prefix = "dubbo.register")
public class DubboRegisterProperties {
    private String ip;
    private int port;
    private String protocol;
}
