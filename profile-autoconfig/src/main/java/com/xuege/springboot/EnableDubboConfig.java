package com.xuege.springboot;

import com.xuege.springboot.autoconfig.DubboConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(DubboConfig.class)
public @interface EnableDubboConfig {

}
